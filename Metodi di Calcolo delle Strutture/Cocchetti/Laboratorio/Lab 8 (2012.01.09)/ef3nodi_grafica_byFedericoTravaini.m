%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           plane_problems.m                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%             Analisi statica di problemi piani negli sforzi              %
%                                                                         %
%                  Giuseppe COCCHETTI  e  Aram CORNAGGIA                  %
%                                                                         %
%                          versione del 07/01/2013                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Il presente software � prodotto ed � da utilizzarsi per finalit�      %
%   di tipo esclusivamente didattico.                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% routine grafica per plottaggio deformata

% definizione parametri per la rappresentazione grafica
dxMin=min(dXY(:,1));
dxMax=max(dXY(:,1));
ddx=dxMax-dxMin;
dxMin=dxMin-ddx/10;
dxMax=dxMax+ddx/10;

dyMin=min(dXY(:,2));
dyMax=max(dXY(:,2));
ddy=dyMax-dyMin;
dyMin=dyMin-ddy/10;
dyMax=dyMax+ddy/10;


figure(1)
title('Configurazione deformata')
axis([dxMin,dxMax,dyMin,dyMax]*1.1)
axis equal
hold on

dAmplif=(ddx/10)/max(abs(du));
for ne=1:nElem
    % nodi dell'elemento finito ne-esimo
    nA=nInc(ne,1);
    nB=nInc(ne,2);
    nC=nInc(ne,3);

    % coordinate dei nodi dell'elemento finito ne-esimo
    dxA=dXY(nA,1);
    dyA=dXY(nA,2);
    dxB=dXY(nB,1);
    dyB=dXY(nB,2);
    dxC=dXY(nC,1);
    dyC=dXY(nC,2);
  
    plot([dxA,dxB,dxC,dxA],[dyA,dyB,dyC,dyA],'k-')

    
    % spostamenti dei nodi dell'elemento finito ne-esimo
    v=nInc(ne,4:9);
    dune=du(v,1);
    
    dudef=dune([1,3,5],1)*dAmplif;
    dvdef=dune([2,4,6],1)*dAmplif;
    plot([dxA+dudef(1),dxB+dudef(2),dxC+dudef(3),dxA+dudef(1)],[dyA+dvdef(1),dyB+dvdef(2),dyC+dvdef(3),dyA+dvdef(1)],'b-')
end


