%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            travi_2D_St.m                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                  Analisi statica di travi continue 2D                   %
%                                                                         %
%                  Giuseppe COCCHETTI  e  Aram CORNAGGIA                  %
%                                                                         %
%                          versione del 05/12/2012                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Il presente software � prodotto ed � da utilizzarsi per finalit�      %
%   di tipo esclusivamente didattico.                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
clc


%%% Fase di input %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dA=[100;
    100;
    100]; % mm^2

dI=[10000;
    10000;
    10000]; % mm^4

dE=[206000;
    206000;
    206000]; % N/mm^2

% matrice delle coordinate
dXY=[    0,  0;
      1000,  0;
      2000,  0;
      3000,  0];
  
nNodi=size(dXY,1);
  
% matrice delle incidenze
nInc=[1,2;
      2,3;
      3,4];

nInc=[nInc, 3*nInc(:,1)-2, 3*nInc(:,1)-1, 3*nInc(:,1), 3*nInc(:,2)-2, 3*nInc(:,2)-1, 3*nInc(:,2)];
nNaste=size(nInc,1);

% carichi distribuiti
dpq=zeros([nNaste,2]);
dpq(:,1)=[0;
          0;
          1];

dpq(:,2)=[.01;
          .01;
          .01];


% definizione gdl vincolati
nNgdlVinc=[1,2,3];


%%% Fase di elaborazione %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nNgdlTot=3*nNodi;
dL=zeros([nNaste,1]);
dK=zeros([nNgdlTot,nNgdlTot]);
dT=zeros([nNgdlTot,1]);
du=zeros([nNgdlTot,1]);
dS=zeros([nNgdlTot,1]);
dNint=zeros([nNaste,21]);
dTint=zeros([nNaste,21]);
dMint=zeros([nNaste,21]);

for ne=1:nNaste
    
  % nodi alle estremit� dell'asta ne-esima
  nA=nInc(ne,1);
  nB=nInc(ne,2);
  dXA=dXY(nA,:);
  dXB=dXY(nB,:);
    
  % lunghezza dell'asta ne-esima
  dL(ne,1)=norm(dXB-dXA);

  % matrice di rigidezza dell'asta ne-esima
  dke=zeros([6,6]);
  dke([1,4],[1,4])=[ 1,-1;
                    -1, 1]*dE(ne,1)*dA(ne,1)/dL(ne,1);

  dke([2,5],[2,5])=[ 12,-12;
                    -12, 12]*dE(ne,1)*dI(ne,1)/dL(ne,1)^3;
                                             
  dke([3,6],[3,6])=[4,2;
                    2,4]*dE(ne,1)*dI(ne,1)/dL(ne,1);
    
  dke([2,5],[3,6])=[ 6, 6;
                    -6,-6]*dE(ne,1)*dI(ne,1)/dL(ne,1)^2;

  dke([3,6],[2,5])=[6,-6;
                    6,-6]*dE(ne,1)*dI(ne,1)/dL(ne,1)^2;

  % assemblaggio della matrice di rigidezza dell'asta ne-esima
  v=nInc(ne,3:8);
  dK(v,v)=dK(v,v)+dke;     

  % assegnazione forze nodali equivalenti a carichi distribuiti
  dfe=zeros([6,1]);
  dfe([1,4],1)=[1;
                1]*dpq(ne,1)*dL(ne,1)/2;
              
  dfe([2,5],1)=[1;
                1]*dpq(ne,2)*dL(ne,1)/2;

  dfe([3,6],1)=[ 1;
                -1]*dpq(ne,2)*dL(ne,1)^2/12;

  dT(v,1)=dT(v,1)+dfe;     
end

% carichi concentrati
dT(11,1)=dT(11,1)+0;     

% spostamenti assegnati
dus=zeros(size(nNgdlVinc'));
 
% definizione gdl liberi
nNgdlLib=[1:nNgdlTot];
nNgdlLib(nNgdlVinc)=[];



%%% Risoluzione del sistema

% partizione matrici e vettori
dKuu=dK(nNgdlLib,nNgdlLib);
dKss=dK(nNgdlVinc,nNgdlVinc);
dKus=dK(nNgdlLib,nNgdlVinc);
dKsu=dK(nNgdlVinc,nNgdlLib);

dTu=dT(nNgdlLib,1);
dTs=dT(nNgdlVinc,1);

% risoluzione
duu=dKuu\(dTu-dKus*dus);
dSs=dKsu*duu+dKss*dus-dTs;


% Ri-ordinamento
% spostamenti
du(nNgdlLib,1)=duu;
du(nNgdlVinc,1)=dus;
du

% reazioni vincolari
dS(nNgdlVinc,1)=dSs;
dS


% calcolo azioni interne
for ne=1:nNaste

  % nodi alle estremit� dell'asta ne-esima
  nA=nInc(ne,1);
  nB=nInc(ne,2);
  dXA=dXY(nA,:);
  dXB=dXY(nB,:);
    
  % matrice di rigidezza dell'asta ne-esima
  dke=zeros([6,6]);
  dke([1,4],[1,4])=[ 1,-1;
                    -1, 1]*dE(ne,1)*dA(ne,1)/dL(ne,1);

  dke([2,5],[2,5])=[ 12,-12;
                    -12, 12]*dE(ne,1)*dI(ne,1)/dL(ne,1)^3;
                                             
  dke([3,6],[3,6])=[4,2;
                    2,4]*dE(ne,1)*dI(ne,1)/dL(ne,1);
    
  dke([2,5],[3,6])=[ 6, 6;
                    -6,-6]*dE(ne,1)*dI(ne,1)/dL(ne,1)^2;

  dke([3,6],[2,5])=[6,-6;
                    6,-6]*dE(ne,1)*dI(ne,1)/dL(ne,1)^2;

                
  % forze nodali equivalenti a carichi distribuiti
  dfe=zeros([6,1]);
  dfe([1,4],1)=[1;
                1]*dpq(ne,1)*dL(ne,1)/2;
              
  dfe([2,5],1)=[1;
                1]*dpq(ne,2)*dL(ne,1)/2;

  dfe([3,6],1)=[ 1;
                -1]*dpq(ne,2)*dL(ne,1)^2/12;
                
  % forze di estremit�
  v=nInc(ne,3:8);
  due=du(v,1);
  dR=dke*due-dfe;
    
  % azione assiale (positiva di trazione) dell'asta ne-esima
  dNint(ne,1)=-dR(1,1);
  ddL=dL(ne,1)/20;
  for np=2:21
    dNint(ne,np)=dNint(ne,np-1)-dpq(ne,1)*ddL;
  end

  
  % azione di taglio (positiva oraria) dell'asta ne-esima
  dTint(ne,1)=dR(2,1);
  for np=2:21
    dTint(ne,np)=dTint(ne,np-1)+dpq(ne,2)*ddL;
  end
  
  
  % azione di momento (positiva se tende fibre opposte ad asse y) dell'asta ne-esima
  dMint(ne,1)=-dR(3,1);
  for np=2:21
    dMint(ne,np)=dMint(ne,np-1)+dTint(ne,np-1)*ddL+dpq(ne,2)*ddL^2/2;
  end
end




figure(1)
title('Configurazione deformata')
hold on
for ne=1:nNaste
  nA=nInc(ne,1);
  nB=nInc(ne,2);
  dXA=dXY(nA,1);
  dXB=dXY(nB,1);
  v=nInc(ne,[3,4,5,6,7,8]);
  due=du(v,1);
  ddL=dL(ne,1)/20;
  dds=[0:.05:1]';
  dudef=[1-dds, dds]*due([1,4],1);
  dvdef=[1-3*dds.^2+2*dds.^3, dL(ne,1)*dds.*(1-2*dds+dds.^2), 3*dds.^2-2*dds.^3, dL(ne,1)*dds.*(-dds+dds.^2)]*due([2,3,5,6],1);
  dudef=dudef+(4*dds.*(1-dds))*dpq(ne,1)*dL(ne,1)^2/8/dE(ne,1)/dA(ne,1);
  dvdef=dvdef+(16*(dds.^2).*((1-dds).^2))*dpq(ne,2)*(dL(ne,1)^4)/384/dE(ne,1)/dI(ne,1);
  plot([dXA,dXB],[0,0],'k-')
  plot([dXA:ddL:dXB]'+dudef,dvdef,'b-')
end




figure(2)
view([0,0,-1])
title('Azione assiale')
hold on
for ne=1:nNaste
  ddL=dL(ne,1)/20;
  nA=nInc(ne,1);
  nB=nInc(ne,2);
  dXA=dXY(nA,1);
  dXB=dXY(nB,1);
  fill([dXA:ddL:dXB,dXB,dXA],[dNint(ne,:),0,0],'r-')
end


figure(3)
view([0,0,-1])
title('Azione di taglio')
hold on
for ne=1:nNaste
  ddL=dL(ne,1)/20;
  nA=nInc(ne,1);
  nB=nInc(ne,2);
  dXA=dXY(nA,1);
  dXB=dXY(nB,1);
  fill([dXA:ddL:dXB,dXB,dXA],[dTint(ne,:),0,0],'r-')
end


figure(4)
view([0,0,-1])
title('Momento flettente')
hold on
for ne=1:nNaste
  ddL=dL(ne,1)/20;
  nA=nInc(ne,1);
  nB=nInc(ne,2);
  dXA=dXY(nA,1);
  dXB=dXY(nB,1);
  fill([dXA:ddL:dXB,dXB,dXA],[dMint(ne,:),0,0],'r-')
end

