%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            retic_2D_St.m                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                Analisi statica di strutture reticolari 2D               %
%                                                                         %
%                  Giuseppe COCCHETTI  e  Aram CORNAGGIA                  %
%                                                                         %
%                          versione del 14/11/2012                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Il presente software � prodotto ed � da utilizzarsi per finalit�      %
%   di tipo esclusivamente didattico.                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
clc


%%% Fase di input %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dA=[1000;
     100;
     100;
    1000;
     100;
     100;
    1000]; % mm^2

dE=[206000;
    206000;
    206000;
    206000;
    206000;
    206000;
    206000]; % N/mm^2

% matrice delle coordinate
dXY=[                  0,                  0;
          2000*cos(pi/3),     2000*sin(pi/3);
                    2000,                  0;
     2000+2000*cos(pi/3),     2000*sin(pi/3);
               2000+2000,                  0];
          
% matrice delle incidenze (matrice che associa i due nodi alle estremit�
% di un'asta, con i gradi di libert� degli stessi)
nInc=[1,2, 1,2, 3,4;
      1,3, 1,2, 5,6;
      2,3, 3,4, 5,6;
      2,4, 3,4, 7,8;
      3,4, 5,6, 7,8;
      3,5, 5,6, 9,10;
      4,5, 7,8, 9,10];

nNaste=7; % numero delle aste
nNodi=5; % numero dei nodi



%%% Fase di elaborazione %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nNgdlTot=2*nNodi; % numero dei gradi di libert�

dL=zeros([nNaste,1]); % lunghezza aste
dCS=zeros([nNaste,2]); % valori coseni e seni
dK=zeros([nNgdlTot,nNgdlTot]); % matrice di rigidezza
dT=zeros([nNgdlTot,1]); % forze nodali
du=zeros([nNgdlTot,1]); % spostamenti
dS=zeros([nNgdlTot,1]); % reazioni vincolari
dN=zeros([nNaste,1]); % azione normale

for ne=1:nNaste
    
    % nodi alle estremit� dell'asta ne-esima
    nA=nInc(ne,1);
    nB=nInc(ne,2);
    dXA=dXY(nA,:);
    dXB=dXY(nB,:);
    
    % lunghezza dell'asta ne-esima
    dL(ne,1)=norm(dXB-dXA);
    
    % cos e sin dell'angolo dell'asta ne-esima
    dCS(ne,:)=(dXB-dXA)/dL(ne,1);
    
    % matrice di rigidezza dell'asta ne-esima
    q=[-dCS(ne,1);
       -dCS(ne,2);
        dCS(ne,1);
        dCS(ne,2)];
    dke=dE(ne,1)*dA(ne,1)/dL(ne,1)*(q*q');
    
    % assemblaggio della matrice di rigidezza dell'asta ne-esima
    v=nInc(ne,3:6);
    dK(v,v)=dK(v,v)+dke; 
    
end
    
% assegnazione forze nodali
dT(6,1)=-50000; % N
dT(10,1)=-40000; % N

% definizione gdl vincolati
nNgdlVinc=[1,2,9,10];

% spostamenti assegnati
dus=[0;
     0;
     0;
     0];
 
% definizione gdl liberi
nNgdlLib=[1:10];
nNgdlLib(nNgdlVinc)=[];



%%% Risoluzione del sistema

% partizione matrici e vettori
dKuu=dK(nNgdlLib,nNgdlLib); % gdl liberi
dKss=dK(nNgdlVinc,nNgdlVinc); % gdl vincolati
dKus=dK(nNgdlLib,nNgdlVinc); % gdl liberi-vincolati
dKsu=dK(nNgdlVinc,nNgdlLib); % gdl vincolati-liberi

dTu=dT(nNgdlLib,1); % forze agenti sui gdl liberi
dTs=dT(nNgdlVinc,1); % forze agenti sui gdl vincolati

% risoluzione
duu=dKuu\(dTu-dKus*dus); % spostamenti gdl liberi
dSs=dKsu*duu+dKss*dus-dTs; % reazioni vincolari


% Ri-ordinamento
% spostamenti
du(nNgdlLib,1)=duu;
du(nNgdlVinc,1)=dus;
du

% reazioni vincolari
dS(nNgdlVinc,1)=dSs;
dS


% calcolo azioni interne (azione assiale)
for ne=1:nNaste
    
    % nodi alle estremit� dell'asta ne-esima
    nA=nInc(ne,1);
    nB=nInc(ne,2);
    dXA=dXY(nA,:);
    dXB=dXY(nB,:);
    
    % lunghezza dell'asta ne-esima
    dL(ne,1)=norm(dXB-dXA);
    
    % cos e sin dell'angolo dell'asta ne-esima
    dCS(ne,:)=(dXB-dXA)/dL(ne,1);
    
    % matrice di rigidezza dell'asta ne-esima
    q=[-dCS(ne,1);
       -dCS(ne,2);
        dCS(ne,1);
        dCS(ne,2)];
    dke=dE(ne,1)*dA(ne,1)/dL(ne,1)*(q*q');
    
    % forze di estremit�
    v=nInc(ne,3:6);
    due=du(v,1);
    dR=dke*due;
    
    % azione assiale (positiva di trazione) dell'asta ne-esima
    dN(ne,1)=dCS(ne,:)*dR(3:4,1);
end

dN
