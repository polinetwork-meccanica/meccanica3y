
%%% fase di input


%%% asta 1

E1=206000;  % N/mm^2
A1=1000;    % mm^2
L1=2000;    % mm
alfa1=0;    % gradi sessagesimali


%%% asta 2

E2=206000;          % N/mm^2
A2=1000;            % mm^2
L2=2000*2/sqrt(3);  % mm
alfa2=30;           % gradi sessagesimali


%%% forze

Fx=20000; % N
Fy=30000; % N





%%% fase di elaborazione


%%% trasformazioni in radianti

alfa1r=alfa1*pi/180;
alfa2r=alfa2*pi/180;


%%% matrice di rigidezza asta 1

k1cap=E1*A1/L1*[cos(alfa1r)^2, cos(alfa1r)*sin(alfa1r);
                cos(alfa1r)*sin(alfa1r), sin(alfa1r)^2];
K1=[k1cap, -k1cap;
    -k1cap, k1cap];


%%% matrice di rigidezza asta 2

k2cap=E2*A2/L2*[cos(alfa2r)^2, cos(alfa2r)*sin(alfa2r);
                cos(alfa2r)*sin(alfa2r), sin(alfa2r)^2];
K2=[k2cap, -k2cap;
    -k2cap, k2cap];


%%% assemblaggio

K=zeros([6,6]);

v1=[2,1];       % nodi all'estremit� dell'asta 1
gdl1=[3,4,1,2]; % gradi di libert�

K(gdl1,gdl1)=K(gdl1,gdl1)+K1;


v2=[3,1];       % nodi all'estremit� dell'asta 2
gdl2=[5,6,1,2]; % gradi di libert�

K(gdl2,gdl2)=K(gdl2,gdl2)+K2;


%%% vettore dei termini noti

T=zeros([6,1]);

T(1,1)=Fx;
T(2,1)=Fy;





%%% risoluzione sistema

U=zeros([6,1]); % vettore dei gradi di libert�
U([1,2],1)=K([1,2],[1,2])\T([1,2],1);


% calcolo delle reazioni vincolari

T([3,4,5,6],1)=K([3,4,5,6],[1,2])*U([1,2],1);
