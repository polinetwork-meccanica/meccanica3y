clear all; close all; clc;

A = [-1,12/5,-1,0.1;
     10/3,1,1,1;
     0.1,-1,0,-1;
     0,0.1,-1,12/5];
 
[L,U,P] = lu(A);

n = size(A,1);

if P ~= eye(n)
    disp('� stato effettuato il Pivoting.')
else
    disp('NON � stato effettuato il Pivoting.')
end

b = [0.5,19/3,-1.9,3/2]';

y=fwsub(L,P*b);
x=bksub(U,y)