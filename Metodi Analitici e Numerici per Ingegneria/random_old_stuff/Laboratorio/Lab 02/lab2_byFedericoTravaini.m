%Laboratorio 2

%Esercizio 1

n=20;
q_0=2;
R=ones(n,1);
A=-diag(R)+diag(R(1:n-1),-1);
A(1,:)=1;

b=zeros(n,1);
b(1)=q_0;

[L,U,P]=lu(A);

if (P==eye(n))
    disp('P = I');    
    figure(1);spy(P)
end;

y=fwsub(L,P*b);
q_1=bksub(U,y);

q_ex=q_0/n*ones(n,1);
err_rel=norm(q_ex-q_1)/norm(q_ex)

res_nor=norm(b-A*q_1)/norm(b)

K1=cond(A)


%ripetiamo l'esercizio con R1=10^3

A2=A;
A2(2,1)=1e+3;

[L2,U2,P2]=lu(A2);


y2=fwsub(L2,P2*b);
q_2=bksub(U2,y2);

K2=cond(A2)


if (P2==eye(n))
    disp('P = I');    
else
    figure(2);spy(P2)
end;


%% Esercizio 2
clear all
close all
home

n=5;
p=[1/n:1/n:1]';

format rat
p

V=vander(p)

x_ex=ones(n,1);
b=V*x_ex;

[L,U,P]=lu(V);

if (P==eye(n))
    disp('P = I');    
else
    figure(1);spy(P)
end;


y=fwsub(L,P*b);
x=bksub(U,y);

format short
err_rel=norm(x_ex-x)/norm(x_ex)

res_nor=norm(b-V*x)/norm(b)

K=cond(V)


% punto 2

err_rel=[];
res_nor=[];
K=[];

format short

for n=3:15
    p=[1/n:1/n:1]';

    V=vander(p);

    x_ex=ones(n,1);
    b=V*x_ex;

    [L,U,P]=lu(V);

    y=fwsub(L,P*b);
    x=bksub(U,y);
    
    err_rel=[err_rel,norm(x_ex-x)/norm(x_ex)];
    res_nor=[res_nor,norm(b-V*x)/norm(b)];
    K=[K,cond(V)];
end;

figure(1);
N=[3:15];
semilogy(N,err_rel,'b-s',N,res_nor,'r-o',N,K,'g-x');
legend('errore relativo','residuo normalizzato','n. di condizionamento');
xlabel('dimensione n');
ylabel('err, res, K');

%% Esercizio 4
n=5;
H=hilb(n);
x_ex=ones(n,1);
b=H*x_ex;
[L,U,P]=lu(H);
y=fwsub(L,P*b);
x=bksub(U,y);

err_rel=norm(x-x_ex)/norm(x_ex)
res_nor = norm(b-H*x)/norm(b)
K = cond(H)

err_rel=[]; res_nor=[]; K=[];
for n=3:15
  H=hilb(n);
  x_ex=ones(n,1);
  b=H*x_ex;
  [L,U,P]=lu(H);
  y=fwsub(L,P*b);
  x=bksub(U,y);
  err_rel=[err_rel, norm(x-x_ex)/norm(x_ex)];
  res_nor=[res_nor, norm(b-H*x)/norm(b)];
  K=[K, cond(H)];
end
N=[3:15];
figure(1);
semilogy(N,err_rel,'-s', N, res_nor,'-o', N, K,'-x')
legend('errore rel.', 'residuo norm.', 'n. di condizionamento')
xlabel('dimensione n');
ylabel('err, res, K');