clc; clear all; close all
%Es1 punto 1 (teorico)

%Es1 punto 2
x0 = 0;
xL = 1;
h = 0.1;

xdof = [x0+h:h:xL-h]';
N = length(xdof);

% matrice
a = 1;    % diffusione
c = 1;    % reazione

U  = ones(N,1);
U1 = ones(N-1,1);

A = a/h  * ( 2*diag(U) - diag(U1,1) - diag(U1,-1) )+ ...
    c*h  * ( 2/3*diag(U) +1/6*diag(U1,1) + 1/6*diag(U1,-1) );

% termine noto
frhs = @(x) -1+0*x;
f = h*frhs(xdof);

%Es1 punto 3
if min(eig(A))>0
    fprintf('definita positiva')
else 
    fprintf('non definita positiva')
end

%Es1 punto 4, condizionamento di A
cond(A)

%Es1 punto 5 e 6
% risoluzione sistema
u = pcg(A,f);

%aggiunta dei bordi
xbc = [x0;xdof;xL];
ubc = [0;u;0];
plot(xbc,ubc,'o-'); hold on

% soluzione esatta
uex = @(x) -1 - ((2*cosh(1) - 1)*sinh(x) - sinh(x+1))/sinh(1);
xdis = linspace(x0,xL,1000);
plot(xdis,uex(xdis),'k')
legend('Soluzione approssimata','Soluzione esatta')
%Es1 punto 7
%calcolo dell'errore in norma L2:
integrandaL2 = @(x) (uex(x) - interp1(xbc,ubc,x)).^2;
errL2 = sqrt(simpcomp(x0,xL,1000,integrandaL2))

%Es1 punto 8,9,10

NN = 10*2.^(1:6);
H = 1./NN;
K = [];
E = [];
for h = H;
    xdof = [x0+h:h:xL-h]';
    N = length(xdof);

    U = ones(N,1);
    U1 = ones(N-1,1);

    A = a/h  * ( 2*diag(U) - diag(U1,1) - diag(U1,-1) )+ ...
        c*h  * ( 2/3*diag(U) +1/6*diag(U1,1) + 1/6*diag(U1,-1) );
    
    K = [K cond(A)];
    
    f = h*frhs(xdof);
    
    %NOTA: usiamo i parametri toll e nmax di pcg altrimenti non converge nel
    %numero massimo di iterazioni fissato di default.
    u = pcg(A,f,1e-6,N);
    
    xbc = [x0;xdof;xL];
    ubc = [0;u;0];

    integrandaL2 = @(x) (uex(x) - interp1(xbc,ubc,x)).^2;
    
    errL2 = sqrt(simpcomp(x0,xL,1000,integrandaL2));
    E = [E errL2 ];

end

figure(2);
loglog(H,E,'bo-',H,H,'k',H,H.^2,'k--')
legend('errL2','H','H^2')
figure(3)
loglog(H,K,'ro-',H,H.^-2,'k')
legend('cond(A)','H^{-2}')

%%
%Es2 punto 1 (teorico)

%Es2 punto 2
clc; clear all; close all
x0 = 0;
xL = 2*pi;
h = (2*pi)/60;

xdof = [x0+h:h:xL-h]';
N = length(xdof);

% matrice
a = 1;    % diffusione

U  = ones(N,1);
U1 = ones(N-1,1);

A = a/h  * ( 2*diag(U) - diag(U1,1) - diag(U1,-1) );

% termine noto
frhs = @(x) -2.*exp(x).*cos(x);

%calcolo del termine F(phi):
%approssimo l'integrale di f*phi_i con l'area del triangolo di base 2h e
%altezza f(xi) [trapezio composito su due sottointervalli]

F=h*frhs(xdof);

%Es2 punto 3 e 4
% risoluzione sistema: ATTENZIONE, è da usare la variabile Nmax!!
u = pcg(A,F,1e-6,N);

%aggiungo i bordi
xbc = [x0;xdof;xL];
ubc = [0;u;0];

figure(1)
plot(xbc,ubc,'o-'); hold on

% soluzione esatta
uex = @(x) exp(x).*sin(x);

xdis = linspace(x0,xL,1000);
plot(xdis,uex(xdis),'k')
