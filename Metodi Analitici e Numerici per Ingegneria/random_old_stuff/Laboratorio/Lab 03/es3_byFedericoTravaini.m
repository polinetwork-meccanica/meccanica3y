%% Esercizio 3
clear; clc;
% Punto 2
n = 32;
A = 4 * diag(ones(n, 1))   ...
  - diag(ones(n-1, 1), 1)  ...
  - diag(ones(n-2, 1), 2)  ...
  - diag(ones(n-1, 1), -1) ...
  - diag(ones(n-2, 1), -2);
[T, niter] = iterqr(A, 10000, 1e-4);
% Estraggo gli autovalori
Dqr = sort(diag(T));
% Calcolo con eig
Deig = sort(eig(A));
% Confronto
fprintf('Differenza con eig: %g\n',norm(Deig-Dqr,inf))
%% Punto 3
condA = Dqr(end) / Dqr(1);
fprintf('Differenza condizionamento: %g\n',abs(condA-cond(A)));
