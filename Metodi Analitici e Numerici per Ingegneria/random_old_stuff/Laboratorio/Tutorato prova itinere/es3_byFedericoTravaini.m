%% Esercizio 3
clear; clc;
%% Punto 1
f = @(x) x .* sin(x);
a = -2; b = 6;
x_fine = linspace(a, b, 1000);
plot(x_fine, f(x_fine), 'LineWidth', 2.0);
grid on;
%% Punto 2
N = 2 : 2 : 6;
Y = zeros(length(N), length(x_fine));
for i = 1:length(N)
  n = N(i);
  x = linspace(a, b, n+1);
  p = polyfit(x, f(x), n);
  Y(i, :) = polyval(p, x_fine);
end
hold all;
plot(x_fine, Y, 'LineWidth', 2.0);
legend('f(x)', 'n = 2', 'n = 4', 'n = 6');
%% Punto 3
E = abs(repmat(f(x_fine), [3 1]) - Y);
figure();
plot(x_fine, E, 'LineWidth', 2.0);
title('Errore');
legend('n = 2', 'n = 4', 'n = 6');
grid on;
disp('         n    Errore');
disp([N', max(E, [], 2)]);
