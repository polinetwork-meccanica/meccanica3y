%% ES1
clc;clear all;close all
f = @(t,y) cos(2*y);

h = 0.02;
t = 0:h:1;
u = zeros(size(t));
u(1) = 0;
for n = 1:length(t)-1
    u(n+1) = u(n)+h*f(t(n),u(n));
end
plot(t,u,'ko-')
yex = @(t) 1/2*asin((exp(4*t)-1)./(exp(4*t)+1));
tdis = linspace(0,1,1000);
hold on; plot(tdis,yex(tdis),'b')

H = 0.02*2.^[0:-1:-4];
ERR = [];
for h = H
    t = 0:h:1;
    u = zeros(size(t));
    u(1) = 0;
    for n = 1:length(t)-1
        u(n+1) = u(n)+h*f(t(n),u(n));
    end
    ERR = [ERR max(abs(u - yex(t)))];
end
figure; loglog(H,H,'b',H,H.^2,'r',H,ERR,'ko-')
legend('H','H^2','ERR')


%% ES2
clc;clear all;close all
f = @(t,y) -2*y;

h = 0.1;
t = 0:h:2;
u = zeros(size(t));
u(1) = 1;
for n = 1:length(t)-1
    u(n+1) = u(n)*(1-h)/(1+h);
end
plot(t,u,'ko-')
yex = @(t) exp(-2*t);
tdis = linspace(0,2,1000);
hold on; plot(tdis,yex(tdis),'b')

H = 0.2*2.^[0:-1:-4]
ERR = [];
for h = H
    t = 0:h:2;
    u = zeros(size(t));
    u(1) = 1;
    for n = 1:length(t)-1
        u(n+1) = u(n)*(1-h)/(1+h);
    end
    ERR = [ERR max(abs(u - yex(t)))];
end
figure; loglog(H,H,'b',H,H.^2,'r',H,ERR,'ko-')
legend('H','H^2','ERR')

%% ES3
clc; clear all; close all

fun = @(t,u,v) -3*u - 4*v;
u(1) = 1;
v(1) = 0;
h = 0.25;

% f(t,u,v) = -3*u -4*v
% u(n+1) = u(n) + h*v(n) + h^2/2*f( t(n), u(n), v(n) )
% v(n+1) = v(n) + h/2( f(t(n),u(n),v(n) ) + f(t(n+1), u(n+1), v(n+1) ) )
% ovvero
% u(n+1) = u(n) + h*v(n) + h^2/2*[ -3*u(n) -4*v(n) ]
% v(n+1) = v(n) + h/2( (-3*u(n) -4*v(n)) + (-3*u(n+1) -4*v(n+1)) )
%        ==> v(n+1)*(1+2*h) = v(n)*(1-2*h) - 3h/2*( u(n)+u(n+1) )

T = 0:h:5;
for n = 1:length(T)-1
    u(n+1) = u(n) + h*v(n) + h^2/2*(-3*u(n) -4*v(n));
    v(n+1) = ( v(n)*(1-2*h) - 3*h/2*(u(n)+u(n+1)) )/(1+2*h);
end
plot(T,u,'ko-')

y = @(t) 3/2*exp(-t) -1/2*exp(-3*t);
tdis = linspace(0,5,1000);
hold on; plot(tdis,y(tdis))

%% Esercizio 4
clc; clear all; close all;
x0 = 0;
xL = 1;
h = 0.1;
xdof = (x0 + h : h : xL)';
N = length(xdof);
% Punto 4
a = 0.1; b = 1.0; gN = 0.0;

e = ones(N, 1); e1 = ones(N-1, 1);
A1 = a/h * ( -diag(e1,1) + 2*diag(e) - diag(e1,-1) );
A1(end, end) = a/h;
A2 = b * ( 0.5*diag(e1,1) - 0.5*diag(e1,-1) );
A2(end, end) = b/2;

A = A1 + A2;
f = @(x) ones(size(x));

F = h * f(xdof);
F(end) = h/2 * f(xdof(end)) + gN;
% Punto 5
lmbda = eig(A)
disp(lmbda)
% Punto 6
disp(cond(A))
% Punto 7
% pcg non va bene perche' non e' simmetrica
u = A \ F;
% Punto 8
uex = @(x) (1 - exp(10*x))/(10*exp(10)) + x;
xfine = linspace(x0, xL, 1001);
figure(1);
plot(xfine, uex(xfine), 'b-');
hold on; grid on;
xbc = [x0; xdof];
ubc = [0; u];
plot(xbc, ubc, 'r-*');
% Punto 9
integrandaL2 = @(x) (uex(x) - interp1(xbc,ubc,x)).^2;
errL2 = sqrt(simpcomp(x0,xL,1000,integrandaL2))

% Punto 10, 11, 12
NN = 10*2.^(1:6);
HH = 1./NN;
KK = zeros(length(NN), 1);
EE = zeros(length(NN), 1);
for k = 1 : length(NN)
  h = HH(k);
  xdof = (x0 + h : h : xL)';
  N = length(xdof);
  
  e = ones(N, 1); e1 = ones(N-1, 1);
  A1 = a/h * ( -diag(e1,1) + 2*diag(e) - diag(e1,-1) );
  A1(end, end) = a/h;
  A2 = b * ( 0.5*diag(e1,1) - 0.5*diag(e1,-1) );
  A2(end, end) = b/2;

  A = A1 + A2;
  f = @(x) ones(size(x));

  F = h * f(xdof);
  F(end) = h/2 * f(xdof(end)) + gN;
    
  KK(k) = cond(A);
  
  u = A \ F;
    
  xbc = [x0; xdof];
  ubc = [0; u];

  integrandaL2 = @(x) (uex(x) - interp1(xbc,ubc,x)).^2;
  errL2 = sqrt(simpcomp(x0,xL,1000,integrandaL2))
  
  EE(k) = errL2;
end

figure(2);
loglog(HH, EE, 'bo-', HH, HH, 'k', HH, HH.^2, 'k--')
legend('errL2','H','H^2')
figure(3)
loglog(HH, KK, 'ro-', HH, HH.^-2, 'k')
legend('cond(A)','H^{-2}')
